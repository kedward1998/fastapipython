import json
import requests

url = 'http://75.119.149.11:30000/Auth/Login'
urlProgram = 'http://75.119.149.11:30002/Atencion/ObtenerListadoAtencionPorUsuario'
urlAddProgram = 'http://75.119.149.11:30002/Atencion/AgregarProgramacion'


def login(data):
    response = requests.post(url, json=data)
    if response.status_code == 200:
        return response
    else:
        return None

def add_program(data, headers):
    response = requests.post(urlAddProgram, json=data, headers= headers)
    print(response.status_code)
    if response.status_code == 200:
        return "success"
    else:
        return None

def get_program_by_user(headers):
    response = requests.post(urlProgram, headers=headers)
    return response