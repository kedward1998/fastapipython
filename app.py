from requests import Request
from fastapi import FastAPI
from models.loginRequest import LoginRequest
from models.addProgramacion import AddProgramRequest
from services.service import login, get_program_by_user, add_program
import json

app = FastAPI()



@app.post('/listado')
def init(request: LoginRequest):
    data = {
        'email': request.email,
        'password': request.password
    }
    token_response = login(data)
    print(token_response.content)

    if token_response is not None:
        token_split = str(token_response.content).split("'")
        token = 'Bearer ' + token_split[1]
        headers = {
            'Authorization': token
        }
        programs = get_program_by_user(headers)
    return json.loads(programs.content)

@app.post('/agregarProgramacion')
def init(form_data: AddProgramRequest):
    data = {
        'tipo': form_data.tipo,
        'actor': form_data.actor,
        'fecha': form_data.fecha,
        'codReferencia': form_data.codReferencia,
        'email': form_data.email,
        'persona': form_data.persona,
        'comentario': form_data.comentario,
        'latitud': form_data.latitud,
        'longitud': form_data.longitud,
        'vehiculo': form_data.vehiculo,
    }
    
    datos = {
        'email': 'cesartaira86@gmail.com',
        'password': '123456'
    }

    token_response = login(datos)

    if token_response is not None:
        
        token_split = str(token_response.content).split("'")
        token = 'Bearer ' + token_split[1]
        headers = {
            'Authorization': token
        }
        programs = add_program(data, headers)
    return programs

