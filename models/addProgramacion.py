from pydantic import BaseModel


class AddProgramRequest(BaseModel):
    tipo: str
    actor: str
    fecha: str
    codReferencia: str
    email: str
    persona: str
    comentario: str
    latitud: str
    longitud: str
    vehiculo: str
